var spa = (function (doc, wndow) {


    let pushHandlerSending = [];
    let pushHandlerCallback = [];
    let submitHandlerCallback = [];

    let handleSubmitForm = function (el, completionCallback) {
        for (let i = 0; i < submitHandlerCallback.length; i++) {
            if ($(el).is(submitHandlerCallback[i].selector)) {
                let result = submitHandlerCallback[i].handler(el, completionCallback);
                if (result === true) {
                    return true;
                } else if (result === false) {
                    return false;
                }
            }
        }

        return true;
    };

    let handleRequestSending = function () {
        for (let i = 0; i < pushHandlerSending.length; i++) {
            pushHandlerSending[i]();
        }
    };

    let handleRequestReceived = function (jqXHR, target) {
        for (let i = 0; i < pushHandlerCallback.length; i++) {

            jqXHR = jqXHR || {
                getResponseHeader: function (key) {
                    return "";
                }
            };

            let responseHeader = jqXHR.getResponseHeader("X-ASRL");
            let author = jqXHR.getResponseHeader("X-Page-Author");
            let title = jqXHR.getResponseHeader("X-Page-Title");

            pushHandlerCallback[i]({
                resourceLocator: responseHeader,
                author: author,
                title: title
            }, target);
        }
    };

    // https://stackoverflow.com/questions/8038726/how-to-trigger-change-when-using-the-back-button-with-history-pushstate-and-pops
    let changePage = function (state, defaultPage, containerPanelSelector, enhancedUrlRoot) {

        if (state === null || (!("url" in state))) { // initial page
            state = {'panelType': "ajax", "url": defaultPage};
        }

        if (state.panelType == null) {
            state.panelType = "ajax";
        }


        let ctx = $(containerPanelSelector);
        if (state.panelType === "iframe") {
            ctx.html($("<iframe   />").attr("src", state.url));
        } else if (state.panelType === "ajax") {

            handleRequestSending();
            $.ajax({
                url: state.url,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-Exclude-Chrome', 'true').setRequestHeader('Accept', 'text/html');
                },
            }).done(function (data, textStatus, jqXHR) {
                ctx.html(data);
                handleRequestReceived(jqXHR, ctx.get(0));
            }).fail(function (jqXHR) {
                ctx.html(jqXHR.responseText || "ERROR");
                // debugger;
                handleRequestReceived(jqXHR, ctx.get(0));
            });
        }
    };

    let goToPage = function (url, e, enhancedUrlRoot) {

        const state = {'panelType': "ajax", "url": url};
        if (e != null && e.target.hasAttribute("data-panel-type")) {
            state.panelType = e.target.getAttribute("data-panel-type")
        }

        const title = ''

        if (e != null) {
            e.preventDefault();
        }

        let newUrl;
        // debugger;
        if (enhancedUrlRoot == null) {
            newUrl = "#?url=" + encodeURIComponent(state.url) + "&panelType=" + encodeURIComponent(state.panelType);
            if (state.panelType === 'ajax') {
                newUrl = "#?url=" + encodeURIComponent(state.url);
            }
        } else {
            let extensionUrl = state.url;
            if (extensionUrl.startsWith(enhancedUrlRoot)) {
                extensionUrl = extensionUrl.substring(enhancedUrlRoot.length);
            }
            let joiner = extensionUrl.indexOf('?') < 0 ? "?" : "&"
            let pathJoiner = "/";
            if (enhancedUrlRoot.endsWith('/') || extensionUrl.startsWith('/')) {
                pathJoiner = '';
            }

            newUrl = enhancedUrlRoot + pathJoiner + extensionUrl + joiner + "panelType=" + encodeURIComponent(state.panelType);
            if (state.panelType === 'ajax') {
                newUrl = enhancedUrlRoot + pathJoiner + extensionUrl;
            }
        }

        history.pushState(state, title, newUrl);

        return false;
    };

    let setupLinkClicking = function (ctx, enhancedUrlRoot) {

        // Handle Regular cases
        $("a[data-panel='main']", ctx).click(function (e) {
            return goToPage(e.target.getAttribute("href"), e, enhancedUrlRoot)
        });

        // Handle html added by js
        let ctxTarget = ctx.get(0);

        let interceptClickEvent = function(e) {
            // debugger;
            let target = $(e.target || e.srcElement);
            
            if (target.is("a[data-panel='main']")){
                e.preventDefault();
                return goToPage(e.target.getAttribute("href"), e, enhancedUrlRoot)
            }
        }
        
        if (ctxTarget.addEventListener) {
            ctxTarget.addEventListener('click', interceptClickEvent);
        } else if (ctxTarget.attachEvent) {
            ctxTarget.attachEvent('onclick', interceptClickEvent);
        }
        
        
        $("form[data-panel='main']", ctx).submit(function (e) {

            let currentForm = this;

            e.preventDefault();
            let linkTarget = e.target.getAttribute("action");

            let queryString = $(currentForm).serialize();

            if (e.target.method.toUpperCase() !== "GET") {

                handleRequestSending();

                let shouldContinueInvoking = handleSubmitForm(e.target, function (data, jqXHR) {
                    if (data == null) {

                        handleRequestReceived(jqXHR, ctx.get(0));
                    } else {

                        ctx.html(data);
                        handleRequestReceived(jqXHR, ctx.get(0));
                    }
                });

                if (shouldContinueInvoking !== false) {
                    $.ajax({
                        url: linkTarget,
                        data: queryString,
                        method: e.target.method,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader('X-Exclude-Chrome', 'true');
                        }
                    }).done(function (data, textStatus, jqXHR) {
                        ctx.html(data);
                        // setupLinkClicking(ctx);
                        handleRequestReceived(jqXHR, ctx.get(0));

                    }).fail(function (jqXHR) {
                        // ctx.html("<b>Error</b>");
                        ctx.html(jqXHR.responseText || "ERROR");
                        // setupLinkClicking(ctx);
                        handleRequestReceived(jqXHR, ctx.get(0));
                    });
                }
                // console.log( $( this ).serialize() );
                return false;
            } else {
                return goToPage(`${linkTarget}?${queryString}`, e, enhancedUrlRoot);
            }
        });
    };

    let handleHashChange = function (defaultPage, placeHolderSelector, enhancedUrlRoot) {
        let hash = window.location.hash;
        while (hash.startsWith("#")) {
            hash = hash.substring(1);
        }
        const urlSearchParams = new URLSearchParams(hash);
        const params = Object.fromEntries(urlSearchParams.entries());
        changePage(params, defaultPage, placeHolderSelector, enhancedUrlRoot);
    }

    let enableNavigation = function (selector, defaultPage, placeHolderSelector, enhancedUrlRoot) {

        $(wndow).on("popstate", function (e) {
            changePage(e.originalEvent.state, defaultPage, placeHolderSelector, enhancedUrlRoot);
        });

        (function (original) { // overwrite history.pushState so that it also calls
            // the change function when called
            history.pushState = function (state) {
                changePage(state, defaultPage, placeHolderSelector, enhancedUrlRoot);
                return original.apply(this, arguments);
            };
        })(history.pushState);

        wndow.onhashchange = function () {
            handleHashChange(defaultPage, placeHolderSelector, enhancedUrlRoot);
        };
        pushHandlerCallback.push(function () {
            setupLinkClicking($(placeHolderSelector), enhancedUrlRoot);
        });

        pushHandlerCallback.push(function (data) {

            // goToPage(data.)
        });

        wndow.addEventListener('load', event => {
            setupLinkClicking($(selector), enhancedUrlRoot);
            handleHashChange(defaultPage, placeHolderSelector, enhancedUrlRoot);
        });
    }


    let defaultNavigator = null;
    return {
        enableNavigation: function (selector, defaultPage, placeHolderSelector, enhancedUrlRoot) {
            // debugger;
            enableNavigation(selector, defaultPage, placeHolderSelector, enhancedUrlRoot);
            defaultNavigator = {
                navigateTo: function (url) {
                    goToPage(url, null, enhancedUrlRoot);
                }
            };
            return defaultNavigator;
        },
        defaultNavigator : function(){return defaultNavigator;},
        onAslrChanging: function (handler) {
            pushHandlerSending.push(handler);
        },
        onAslrChanged: function (handler) {
            pushHandlerCallback.push(handler);
        },
        onFormSubmitting: function (selector, handler) {
            submitHandlerCallback.push({selector: selector, handler: handler});
        }
    };
})(document, window);


spa.onAslrChanged(function (x) {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
});


// "body"